const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    color:"",
    countDownDay: "",
    countDownHour: "",
    countDownMinute: "",
    countDownSecond: "",
    setting:{},
  },
  ready: function (options) {
    var that=this;
    var oldData = that.data;
    app.globalData.consoleFun("=====activitiesGoods组件-导航=====",[that.data.data])
    that.setData({ setting: app.globalData.setting.platformSetting})
    if (oldData.data.relateBean.promotionStatus == 1&&oldData.data.relateBean.endDate) {
      var interval = setInterval(function () {
        var t1 = oldData.data.relateBean.endDate;
        var d1 = t1.replace(/\-/g, "/");
        var date1 = new Date(d1);
        var totalSecond = parseInt((date1 - new Date()) / 1000);
        var second = totalSecond;
        var day = Math.floor(second / 3600 / 24);
        var dayStr = day.toString();
        if (dayStr.length == 1) dayStr = '0' + dayStr;
        var hr = Math.floor((second - day * 3600 * 24) / 3600);
        var hrStr = hr.toString();
        if (hrStr.length == 1) hrStr = '0' + hrStr;
        var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
        var minStr = min.toString();
        if (minStr.length == 1) minStr = '0' + minStr;
        var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
        var secStr = sec.toString();
        if (secStr.length == 1) secStr = '0' + secStr;
        this.setData({
          countDownDay: dayStr,
          countDownHour: hrStr,
          countDownMinute: minStr,
          countDownSecond: secStr,
        });
        totalSecond--;
        if (totalSecond < 0) {
          clearInterval(interval);
          wx.showToast({
            title: '活动已结束',
          });
          this.setData({
            countDownDay: '00',
            countDownHour: '00',
            countDownMinute: '00',
            countDownSecond: '00',
          });
        }
      }.bind(this), 1000);
    }
    if (oldData.data.relateBean.promotionStatus == 0&&oldData.data.relateBean.startDate) {
      var interval = setInterval(function () {
        var t1 = oldData.data.relateBean.startDate;
        var d1 = t1.replace(/\-/g, "/");
        var date1 = new Date(d1);
        var totalSecond = parseInt((date1 - new Date()) / 1000);
        var second = totalSecond;
        var day = Math.floor(second / 3600 / 24);
        var dayStr = day.toString();
        if (dayStr.length == 1) dayStr = '0' + dayStr;
        var hr = Math.floor((second - day * 3600 * 24) / 3600);
        var hrStr = hr.toString();
        if (hrStr.length == 1) hrStr = '0' + hrStr;
        var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
        var minStr = min.toString();
        if (minStr.length == 1) minStr = '0' + minStr;
        var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
        var secStr = sec.toString();
        if (secStr.length == 1) secStr = '0' + secStr;
        this.setData({
          countDownDay: dayStr,
          countDownHour: hrStr,
          countDownMinute: minStr,
          countDownSecond: secStr,
        });
        totalSecond--;
        if (totalSecond < 0) {
          clearInterval(interval);
          this.setData({
            countDownDay: '00',
            countDownHour: '00',
            countDownMinute: '00',
            countDownSecond: '00',
          });
        }
      }.bind(this), 1000);
    }
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    },
    clickLink: function (e) {
      console.log(e)
      var oldData = this.data;
      if (oldData.data.relateBean.promotionStatus == 1) {
        console.log("=====已开始的活动========")
        var a = "product_detail.html?productId=" + e.currentTarget.dataset.id;
        app.globalData.linkEvent(a);
      }else if (oldData.data.relateBean.promotionStatus == 0) {
        console.log("=====未开始的活动========")
        var a = "promotion_detail.html?promotionId=" + e.currentTarget.dataset.id;
        app.globalData.linkEvent(a);
      }else if (oldData.data.relateBean.promotionStatus == 2) {
        console.log("=====已结束的活动========")
        var a = "product_detail.html?productId=" + e.currentTarget.dataset.id;
        app.globalData.linkEvent(a);
      }
    },
    clickLink1: function (e) {
      var oldData = this.data;
      if (oldData.data.relateBean.promotionStatus == 1) {
        wx.navigateTo({
          url: '../../pageTab/tunzai/teMai/index?promotionId=' + e.currentTarget.dataset.id,
        })
      }
      if (oldData.data.relateBean.promotionStatus == 0) {
        var a = "promotion_detail.html?promotionId=" + e.currentTarget.dataset.id;
        app.globalData.linkEvent(a);
      }
    },
  },
})