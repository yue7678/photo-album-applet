const app = getApp();
Component({
  properties: {
    receiveData: {
      type: Object,
      value: 'default value',
    },
  },
  data: {
    setting: null, 
    courseList: null,
    limitState:0,
    params:null,
    listPage:{
        page: 1,
        pageSize: 20,
        totalSize: 0,
    },
  },
  ready: function () {
    let that = this;
    this.setData({ setting: app.globalData.setting })
    console.log("====courseList-data=====", that.data.receiveData);
    that.initSetting(that.data.receiveData);
  },
  methods: {
    setDataFun:function(data){
        let that=this;
        console.log("setDataFun",data)
        that.setData({params: data})
        that.getData(data)
    },
    /* 获取数据 */
    getData: function (param) {
      var that = this
      let params=Object.assign({},param,{
      })
      console.log("===params==",params)
      var customIndex = app.globalData.AddClientUrl("/wx_find_courses_instances.html", params)
      app.globalData.showToastLoading('loading', true)
      wx.request({
        url: customIndex.url,
        header: app.globalData.header,
        success: function (res) {
          wx.hideLoading()
          console.log(res.data)
          let data=res.data.relateObj.result
          that.data.listPage.pageSize = res.data.relateObj.pageSize
          that.data.listPage.page = res.data.relateObj.curPage
          that.data.listPage.totalSize = res.data.relateObj.totalSize
          let dataArr = that.data.courseList;
          let tagArray=[];
          for (let i = 0; i < data.length; i++) {
            if (data[i].tags && data[i].tags != '') {
              tagArray = data[i].tags.slice(1, -1).split("][")
              data[i].tagArray = tagArray;
            }
          }
          console.log("===data====",data)
          if (param.page == 1) {
            dataArr = []
          }
          if (!data || data.length == 0) {
            that.setData({ courseList: [] })
          } else {
            if (dataArr == null) { dataArr = [] }
            dataArr = dataArr.concat(data)
            that.setData({ courseList: dataArr })
          }
          wx.hideLoading()
        },
        fail: function (res) {
          console.log("fail")
          wx.hideLoading()
          app.globalData.loadFail()
        }
      })
    },
    calling: function (e) {
      console.log('====e===', e)
      let phoneNumber = e.currentTarget.dataset.phonenumber
      wx.makePhoneCall({
        phoneNumber: phoneNumber, //此号码并非真实电话号码，仅用于测试
        success: function () {
          console.log("拨打电话成功！")
        },
        fail: function () {
          console.log("拨打电话失败！")
        }
      })
    },
    toIndex() {
      app.globalData.toIndex()
    },
    tolinkUrl: function (e) {
      let linkUrl = e.currentTarget.dataset.link
      app.globalData.linkEvent(linkUrl)
    },
    initSetting(data) {
      let that=this;
      if (data.jsonData ) {
          that.data.params=Object.assign({},that.data.params,data.jsonData)
          that.getData(that.data.params)
          if(data.jsonData.count){
              that.setData({ limitState: data.jsonData.count })
          }
      }
      console.log("====courseList-limitState=====", that.data.limitState)
      console.log("====courseList-params=====", that.data.params)
    },
    pullDownRefreshFun: function () {
      this.data.params.name = ""
      this.data.listPage.page = 1
      this.data.params.page = 1
      this.getData(this.data.params)

    },
    reachBottomFun: function () {
      var that = this
      if (that.data.listPage.totalSize > that.data.listPage.curPage * that.data.listPage.pageSize) {
        that.data.listPage.page++
        that.data.params.page++
        that.getData(that.data.params);
      }else{
          wx.showToast({
            title: "到底了~",
            image: '/wxcomponents/images/icons/tip.png',
            duration: 1000
          });
      }
    },
  }
})